import CLIENT_ID from './constants';

export async function getImages(page = 0) {
  const option = {
    headers: {
      authorization: `Client-ID ${CLIENT_ID}`
    }
  };
  const url = `https://api.imgur.com/3/gallery/t/polandball/${page}`;

  const response = await fetch(url, option);
  const json = await response.json();

  return { json };
}

export async function getSearchedImages(query = 'a', page = 0) {
  const option = {
    headers: {
      authorization: `Client-ID ${CLIENT_ID}`
    }
  };
  const url = `https://api.imgur.com/3/gallery/search/${page}?q='title: ${query} tag: polandball'`;

  const response = await fetch(url, option);
  const json = await response.json();

  return { json };
}

export async function getSingleImage(id) {
  const option = {
    headers: {
      authorization: `Client-ID ${CLIENT_ID}`
    }
  };
  const url = `https://api.imgur.com/3/gallery/${id}`;

  const response = await fetch(url, option);
  const json = await response.json();

  return { json };
}
