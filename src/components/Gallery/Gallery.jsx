import React from 'react';
import PropTypes from 'prop-types';
import GalleryItem from '../GalleryItem/GalleryItem';
import './Gallery.css';

const propTypes = {
  items: PropTypes.arrayOf(PropTypes.object).isRequired
};

function Gallery({ items }) {
  const galleryItems = items.map((item) => {
    const imgID = item.cover || item.id;
    const link = `https://i.imgur.com/${imgID}_d.jpg?maxwidth=486&shape=thumb&fidelity=high`;

    return <GalleryItem key={item.id} id={item.id} title={item.title} url={link} />;
  });

  return (
    <section className="gallery">
      {galleryItems}
    </section>
  );
}

Gallery.propTypes = propTypes;

export default Gallery;
