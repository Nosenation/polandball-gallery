import React from 'react';
import PropTypes from 'prop-types';
import './Header.css';

const propTypes = {
  title: PropTypes.string.isRequired
};


function Header({ title }) {
  return (
    <header className="header">
      <h1>{title}</h1>
    </header>
  );
}

Header.propTypes = propTypes;

export default Header;
