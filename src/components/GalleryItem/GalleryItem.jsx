import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import './GalleryItem.css';

const propTypes = {
  id: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired
};

function GalleryItem(props) {
  return (
    <Link to={`/galleryItem/${props.id}`}>
      <div style={{ backgroundImage: `url(${props.url})` }} className="galleryItem">
        <span className="galleryItem__title">{props.title}</span>
      </div>
    </Link>
  );
}

GalleryItem.propTypes = propTypes;

export default GalleryItem;
