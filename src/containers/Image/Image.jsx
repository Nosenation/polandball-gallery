import React from 'react';
import PropTypes from 'prop-types';
import Header from '../../components/Header/Header';
import { getSingleImage } from '../../api';
import './Image.css';

const propTypes = {
  match: PropTypes.object.isRequired
};

class Image extends React.Component {
  constructor(props) {
    super(props);

    this.getData = this.getData.bind(this);

    this.state = {
      image: {}
    };
  }

  componentDidMount() {
    this.getData();
  }

  async getData() {
    const { id } = this.props.match.params;
    const { json } = await getSingleImage(id);

    this.setState({
      image: json.data
    });
  }

  render() {
    const { cover, description, id, title } = this.state.image;
    const imgID = cover || id;
    const link = `https://i.imgur.com/${imgID}.jpg?maxwidth=1100`;
    const headerTitle = title || description;

    if (!title) {
      return (
        <article>
          Loading ...
        </article>
      );
    }

    return (
      <article>
        <Header title={headerTitle} />
        <img className="image" alt={title} src={link} />
      </article>
    );
  }
}


Image.propTypes = propTypes;

export default Image;
