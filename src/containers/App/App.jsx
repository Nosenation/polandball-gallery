import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import HomePage from '../HomePage/HomePage';
import Image from '../Image/Image';
import './App.css';

function App() {
  return (
    <div className="app">
      <Router>
        <Switch>
          <Route path="/" exact component={HomePage} />
          <Route path="/galleryItem/:id" component={Image} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
