import React from 'react';
import PropTypes from 'prop-types';
import './Search.css';

const propTypes = {
  onSearch: PropTypes.func.isRequired
};

class Search extends React.Component {
  constructor(props) {
    super(props);
    this.onClickHandler = this.onClickHandler.bind(this);
    this.onInputChange = this.onInputChange.bind(this);

    this.state = {
      value: ''
    };
  }

  onClickHandler() {
    this.props.onSearch(this.state.value);
  }

  onInputChange(e) {
    this.setState({ value: e.target.value });
  }

  render() {
    return (
      <div className="search">
        <input
          className="search__input"
          type="text"
          placeholder="Search..."
          value={this.state.value}
          onChange={this.onInputChange}
        />
        <button className="search__button" onClick={this.onClickHandler}>Search</button>
      </div>
    );
  }
}

Search.propTypes = propTypes;

export default Search;
