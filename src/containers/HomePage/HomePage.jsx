import React from 'react';
import Search from '../Search/Search';
import Gallery from '../../components/Gallery/Gallery';
import Header from '../../components/Header/Header';
import { getImages } from '../../api';

class HomePage extends React.Component {
  constructor(props) {
    super(props);
    this.onSearch = this.onSearch.bind(this);
    this.onScrollStart = this.onScrollStart.bind(this);
    this.respondToScroll = this.respondToScroll.bind(this);
    this.getData = this.getData.bind(this);

    this.fetching = false;
    this.imagesCount = 0;
    this.needToFetch = true;
    this.pageNumber = 0;
    this.scrollInterval = null;
    this.totalImagesCount = 0;

    this.state = {
      images: [],
      filteredImages: []
    };
  }

  componentDidMount() {
    this.getData();
    window.addEventListener('scroll', this.onScrollStart);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.onScrollStart);
  }

  // TODO: Change to API call
  onSearch(value) {
    const filteredImages = this.state.images.filter((item) => {
      const lowered = item.title.toLowerCase();

      return lowered.includes(value);
    });

    this.setState({
      filteredImages
    });
  }

  onScrollStart() {
    if (this.scrollInterval) {
      clearInterval(this.scrollInterval);
    }

    this.scrollInterval = setInterval(this.respondToScroll, 100);
  }

  async getData() {
    const { json } = await getImages(this.pageNumber);

    this.fetching = false;
    this.imagesCount += json.data.items.length;
    this.totalImagesCount = json.data.total_items;

    if (this.totalImagesCount - this.imagesCount > 0) {
      this.pageNumber += 1;
    } else {
      this.needToFetch = false;
    }

    this.setState({
      images: this.state.images.concat(json.data.items)
    });
  }

  respondToScroll() {
    const documentHeight = document.body.clientHeight;
    const offsetFromTop = window.pageYOffset;
    const offsetLimit = Math.floor(documentHeight * 0.7);
    const windowHeight = window.innerHeight;

    if (!this.fetching && this.needToFetch && windowHeight + offsetFromTop >= offsetLimit) {
      this.fetching = true;
      this.getData();
    }
  }

  render() {
    const { filteredImages, images } = this.state;
    const items = (filteredImages.length) ? filteredImages : images;

    return (
      <article>
        <Header title="Polandball Gallery" />
        <Search onSearch={this.onSearch} />
        <Gallery items={items} />
      </article>
    );
  }
}

export default HomePage;
