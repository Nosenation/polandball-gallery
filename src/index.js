import React from 'react';
import ReactDOM from 'react-dom';
import App from './containers/App/App';
import registerServiceWorker from './util/registerServiceWorker';
import './index.css';

ReactDOM.render(React.createElement(App), document.getElementById('root'));
registerServiceWorker();
